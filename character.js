export const player = Object.assign({
  experience: 0,
  health: 7
}, JSON.parse(localStorage.getItem('player') || '{}'))

export function save () {
  localStorage.setItem('player', JSON.stringify(player))
}

export function recover () {
  player.health = player.maxHealth
}
