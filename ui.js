import { player } from './character.js'
import { calculateLevel, getRequired } from './levels.js'
import { get, toggle } from './variables.js'

const output = document.getElementById('output')

export function refreshExperience () {
  const progress = document.querySelector('progress')
  const level = calculateLevel(player.experience)
  const length = getRequired(level) - getRequired(level - 1)

  const value = player.experience - getRequired(level - 1)
  progress.max = length
  progress.value = value
}

let healthState = player.health

export function refreshHealth () {
  if (player.health > healthState) {
    healthState++
  }
  if (player.health < healthState) {
    healthState--
  }
  document.getElementById('health').textContent = Math.max(healthState, 0)
  document.getElementById('maxHealth').textContent = player.maxHealth
  if (healthState !== player.health) {
    setTimeout(refreshHealth, 100)
  }
}

export function clear () {
  output.innerHTML = ''
}

export function showList (items, options) {

  options = Object.assign({ slide: true }, options)
  const itemVelocity = 50
  const transitionDuration = 500
  const commonStyle = {
    display: 'block',
    width: '200px',
    textAlign: 'center',
    borderRadius: '10px',
    border: '1px solid #000',
    fontWeight: 'bold',
    padding: '20px',
    transition: `background 300ms, opacity ${transitionDuration}ms`
  }
  
  return new Promise(resolve => {
    const output = options.parent ?? document.getElementById('output')
    const list = document.createElement('div')
    list.className = 'list'
    let index = 0
    items = items.filter(i => (i.isVisible || (() => true))())

    for (const item of items) {
      item.element = createItemElement(item)
      if (options.slide) {
        item.element.style.transform = 'translate(-300px)'
      }
      item.element.style.transition = `background 300ms, opacity ${transitionDuration}ms, transform ${itemVelocity * items.length}ms`,
      setTimeout(() => item.element.style.transform = '', index * itemVelocity + 5)
      index++

      if (item.lights) {
        let counter = 0
        for (const varName of item.lights) {
          if (get(varName)) {
            counter++
          } else if (varName[0] === '!' && !get(varName.substring(1))) {
            counter++
          }
        }
        let indicator = ''
        for (let i = 0; i < item.lights.length; i++) {
          if (i < counter) {
            indicator += '●'
          } else {
            indicator += '○'
          }
        }
        item.element.textContent = `${item.label} ${indicator}`
      }

      if (item.toggle) {
        const container = document.createElement('div')
        container.className = 'checkbox'
        const tick = document.createElement('div')
        tick.className = 'tick'
        container.append(tick)
        item.element.append(container)
        if (get(item.toggle)) {
          container.classList.add('checked')
        } else {
          container.classList.remove('checked')
        }
        item.element.addEventListener('click', () => {
          const value = toggle(item.toggle)
          if (value) {
            container.classList.add('checked')
          } else {
            container.classList.remove('checked')
          }
        })
      } else {
        let stop = false
        if (item.sublist) {
          item.element.addEventListener('click', () => {
            if (stop) return

            if (item.element.classList.contains('active')) {
              item.sublistContainer.style.height = 0;
              items.forEach(i => i.element.classList.remove('disabled'))
              item.element.classList.remove('active')
              stop = true
              setTimeout(() => {
                list.removeChild(item.sublistContainer)
                stop = false
              }, item.sublist.length * 100)
              return
            }
            items.forEach(i => i.element.classList.add('disabled'))
            item.element.classList.add('active')
            const container = document.createElement('div')
            for (const subitem of item.sublist) {
              subitem.element = createItemElement(subitem)
              container.appendChild(subitem.element)
              subitem.element.addEventListener('click', async () => {
                const [parentItem, selected] = await Promise.all([animate(output, list, item), animate(output, container, subitem)])
                if (subitem.action) {
                  resolve(await subitem.action({ parentItem, selected }))
                } else {
                  resolve(subitem)
                }
              })
            }
            list.insertBefore(container, item.element.nextSibling)
            const originalHeight = container.offsetHeight + 10 // margins
            Object.assign(container.style, {
              overflow: 'hidden',
              height: 0,
              transition: `height ${item.sublist.length * 100}ms`
            })
            setTimeout(() => container.style.height = originalHeight + 'px', 1)
            item.sublistContainer = container
          })
        } else {
          item.element.addEventListener('click', async () => {
            if (stop || item.element.classList.contains('disabled')) {
              return
            }
            const selected = item.justDisappear ? 
              await disappear(list, item) : 
              await animate(output, list, item)
            if (item.action) {
              resolve(await item.action({ selected }))
            } else {
              resolve(item)
            }
            if (!item.justDisappear && list.parentNode) {
              list.parentNode.removeChild(list)
            }
            if (options.hideSelection) {
              makeDisappear(selected)
            }
          })
        }
      }
      list.appendChild(item.element)
    }
    output.appendChild(list)
    if (options.onInit) {
      options.onInit(list)
    }
  })

  function createItemElement (item) {
    const element = document.createElement('a')
    element.className = 'item'
    element.textContent = item.label

    Object.assign(element.style, {
      ...commonStyle,
      cursor: 'pointer',
      margin: '5px auto',
      position: 'relative'
    })
    return element
  }

  function disappear (list, item) {
    return new Promise(resolve => {
      const element = item.element
      Object.assign(element.style, {
        transition: 'height 300ms, padding 300ms, margin 300ms',
        overflow: 'hidden',
        height: 0,
        paddingTop: 0,
        paddingBottom: 0,
        marginTop: 0,
        marginBottom: 0
      })
      setTimeout(() => {
        list.removeChild(element)
        resolve()
      }, 300)
    })
  }

  function animate (parent, list, item) {
    return new Promise(resolve => {
      const selected = document.createElement('a')
      const rect = item.element.getBoundingClientRect()
      Object.assign(selected.style, {
        ...commonStyle,
        borderColor: 'transparent',
        position: 'absolute',
        top: `${rect.top}px`,
        left: `${rect.left}px`
      })
      selected.textContent = item.element.textContent
      document.body.appendChild(selected)

      const trash = []

      for (let i = 0; i < 4; i++) {
        const outline = document.createElement('a')
        Object.assign(outline.style, {
          position: 'absolute',
          top: `${rect.top}px`,
          left: `${rect.left}px`,
          width: `${rect.width}px`,
          height: `${rect.height}px`,
          border: '1px solid #000',
          borderRadius: '10px',
          transition: `transform ${transitionDuration}ms, opacity ${transitionDuration}ms`
        })
        
        document.body.appendChild(outline)
        setTimeout(() => {
          outline.style.transform = 'scale(1.5)'
          outline.style.opacity = 0
        }, i * 150)

        trash.push(outline)
      }

      list.querySelectorAll('.item').forEach(element => {
        element.style.opacity = 0
        element.style.cursor = 'default'
      })

      list.style.height = `${list.offsetHeight}px`
      list.style.paddingTop = 0
      list.style.transition = `height ${transitionDuration}ms`
      
      setTimeout(() => resolve(selected), 800)
      setTimeout(() => {
        for (const e of trash) {
          e.parentNode.removeChild(e)
        }
      }, 2000)
    })
  }
}

export const ANIMATION_DURATION = 1000

export function makeDisappear(element, time) {
  time = time ?? 1000
  return new Promise((resolve) => {
    element.style.transition = `opacity ${time}ms`
    setTimeout(() => {
      element.style.opacity = 0
    }, 1)
    setTimeout(() => {
      element.parentNode.removeChild(element)
      resolve()
    }, time)
  })
}

export function whiteOut() {
  const STEP = 10
  const DURATION = 500
  return new Promise((resolve) => {
    const canvas = document.createElement('canvas')
    Object.assign(canvas.style, {
      position: 'absolute',
      top: 0,
      left: 0,
      transition: 'opacity 200ms',
      zIndex: 50
    })
    canvas.width = window.innerWidth,
    canvas.height = window.innerHeight
    document.body.appendChild(canvas)

    const context = canvas.getContext('2d')
    context.fillStyle = '#fff'
    context.strokeStyle = '#777'

    let progress = 0
    const points = Array(1000).fill(0).map(_ => Math.floor(Math.random() * 20))
    const tid = setInterval(() => {
      progress += canvas.width * (STEP / DURATION)
      context.beginPath()
      context.moveTo(canvas.width, 0)
      let i = 0
      for (let y = 0; y < canvas.height; y += 10) {
        const x = canvas.width - progress - points[i]
        context.lineTo(x, y)
        i++
      }
      context.lineTo(canvas.width, canvas.height)
      context.fill()
      context.closePath()
      if (progress > canvas.width) {
        clearTimeout(tid)
        resolve(canvas)
      }
    }, STEP)
  })
}

export function showBigText (text) {
  return new Promise((resolve) => {
    const div = document.createElement('div')
    div.textContent = text
    Object.assign(div.style, {
      position: 'absolute',
      fontSize: '32pt',
      fontWeight: 'bold',
      top: '30%',
      left: 'calc(50% - 80px)',
      opacity: 0,
      transition: 'transform 5000ms, opacity 1000ms'
    })
    document.body.appendChild(div)
    setTimeout(() => {
      div.style.transform = 'scale(80%)'
      div.style.opacity = 1
    }, 1)
    setTimeout(() => {
      div.style.transform = 'scale(30%)'
      div.style.opacity = 0
    }, 4000)
    setTimeout(() => {
      document.body.removeChild(div)
      resolve()
    }, 5000)
  })
}

export function showButtons (onConfirm, onCancel) {
  const nextButton = document.getElementById('next')
  nextButton.style.opacity = 1
  nextButton.addEventListener('click', () => {
    if (onConfirm()) {
      nextButton.style.opacity = 0
      cancelButton.style.opacity = 0
    }
  }, { once: true })
  const cancelButton = document.getElementById('cancel')
  cancelButton.style.opacity = 1
  cancelButton.addEventListener('click', () => {
    if (onCancel()) {
      nextButton.style.opacity = 0
      cancelButton.style.opacity = 0
    }
  }, { once: true })
}

export function showDescription (text, effect) {
  return new Promise((resolve) => {
    const button = document.getElementById('next')
    button.style.opacity = 1
    output.innerHTML = ''

    if (text instanceof Array) {
      const texts = text.slice()
      next()
      function next() {
        
        const line = texts.shift()
        const element = document.createElement('div')
        const delay = line.length * 50
        element.innerHTML = line
        output.appendChild(element)

        if (effect === 'fade') {
          button.style.opacity = 0
          element.style.opacity = 0
          element.style.transition = `opacity ${delay}ms`
          setTimeout(() => element.style.opacity = 1, 1)
          if (texts.length !== 0) {
            setTimeout(next, delay)
          } else {
            button.style.opacity = 1
            button.addEventListener('click', finish, { once: true })
          }
        } else {
          if (texts.length !== 0) {
            if (effect !== 'print') {
              button.addEventListener('click', next, { once: true })
            } else {
              next()
            }
          } else {
            if (effect !== 'print') {
              button.addEventListener('click', finish, { once: true })
            }
          }
        }
        
      }
    } else {
      const element = document.createElement('div')
      element.innerHTML = text
      output.appendChild(element)
      if (effect === 'big') {
        element.style.textAlign = 'center'
        element.style.fontWeight = 'bold'
        element.style.fontSize = '24pt'
        element.style.marginTop = '40%'
      }
      button.addEventListener('click', finish, { once: true })
    }

    function finish() {
      button.style.transition = ``
      button.style.opacity = 0
      output.innerHTML = ''
      resolve()
    }
  })
}

export function setTitle(title) {
  const crop = document.getElementById('crop')
  const rect = output.getBoundingClientRect()
  crop.style.left = `${rect.left}px`
  crop.style.top = `${rect.top}px`
  const label = document.getElementById('title')
  label.innerText = title
  crop.style.width = `${label.offsetWidth}px`
  setTimeout(() => {
    label.style.opacity = 1
  }, 1)
}

export function wait(duration) {
  return new Promise((resolve) => {
    setTimeout(resolve, duration)
  })
}

export async function mergeElements(source, destination, time) {
  const sourceStyle = getComputedStyle(source)
  destination.style.fontSize = sourceStyle.fontSize
  destination.style.fontWeight = sourceStyle.fontWeight
  destination.style.visibility = 'hidden'
  destination.textContent = source.textContent

  const rect = destination.getBoundingClientRect()
  source.style.width = `${rect.width}px`
  source.style.transition = `all ${time}ms`
  source.style.padding = 0
  source.style.top = `${rect.top}px`
  source.style.left = `${rect.left}px`
  source.style.borderWidth = 0
  source.style.zIndex = 10;
  await wait(time)
  destination.style.visibility = 'visible'
  source.parentNode.removeChild(source)
}

export async function tell(name, messages) {
  setTitle(name)
  return showDescription(messages)
}

export async function showYes() {
  setTitle('')
  return showDescription('YES', 'big')
}

export async function showNo() {
  setTitle('')
  return showDescription('NO', 'big')
}
