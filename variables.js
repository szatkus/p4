const observers = []

export function set (key, value) {
  if (value === undefined) {
    value = true
  }
  localStorage.setItem(key, value)
  observers.filter(o => o.key === key).forEach(o => o.callback(value))
  return value
}

export function get (key) {
  const value = localStorage.getItem(key)
  if (value === 'false') {
    return false
  }
  return value
}

export function toggle (key) {
  return set(key, !get(key))
}
