const DEBUG = false

if (DEBUG) {
  const loadButton = document.createElement('button')
  loadButton.textContent = 'Load'
  loadButton.addEventListener('click', () => {
    const state = JSON.parse(localStorage.getItem('savestate' + slotInput.value))
    for (const key in localStorage) {
      if (key.indexOf('savestate') !== 0) {
        localStorage.removeItem(key)
      }
    }
    for (const key in state) {
      localStorage.setItem(key, state[key])
    }
    location.reload()
  })
  const saveButton = document.createElement('button')
  saveButton.textContent = 'Save'
  saveButton.addEventListener('click', () => {
    const state = Object.assign({}, localStorage)
    for (const key in state) {
      if (key.indexOf('savestate') === 0) {
        delete state[key]
      }
    }
    localStorage.setItem('savestate' + slotInput.value, JSON.stringify(state))
  })
  const clearButton = document.createElement('button')
  clearButton.textContent = 'Clear'
  clearButton.addEventListener('click', () => {
    for (const key in localStorage) {
      if (key.indexOf('savestate') !== 0) {
        localStorage.removeItem(key)
      }
    }
    location.reload()
  })
  const slotInput = document.createElement('input')
  slotInput.value = '0'
  document.body.insertBefore(slotInput, document.body.firstChild)
  document.body.insertBefore(loadButton, document.body.firstChild)
  document.body.insertBefore(saveButton, document.body.firstChild)
  document.body.insertBefore(clearButton, document.body.firstChild)
  
}
