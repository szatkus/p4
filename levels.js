import { player, save } from './character.js'
import { ANIMATION_DURATION, refreshHealth } from './ui.js'
import { get } from './variables.js'

const FACTOR = 7

export function getRequired (level) {
  if (level === 0) {
    return 0
  }
  return Math.floor(49 * (1.14 ** level) - 48)
}

export function calculateLevel (xp) {
  let level = 1
  while (xp >= getRequired(level)) {
    level++
  }
  return level
}

export function recalculate () {
  const level = calculateLevel(player.experience)
  player.maxHealth = level + 6
  player.attack = level + 6
  player.defense = level + 6
  player.speed = level + 6
  player.accuracy = level + 6
  player.evasion = level + 6
  if (get('health10')) {
    player.maxHealth += 10
  }
}

recalculate()

export function addExperience (increase) {
  return new Promise(resolve => {
    const progress = document.querySelector('progress')
    progress.style.visibility = 'visible'
    const base = player.experience
    player.experience += increase
    const level = calculateLevel(base)
    const finalLevel = calculateLevel(base + increase)
    const length = getRequired(level) - getRequired(level - 1)
    const velocity = (getRequired(finalLevel) - getRequired(finalLevel - 1)) / ANIMATION_DURATION

    const value = base - getRequired(level - 1)
    progress.max = length
    progress.value = value

    const start = +new Date()
    function update () {
      let delta = (new Date() - start) * velocity
      if (delta > increase) {
        delta = increase
        recalculate()
        refreshHealth()
        resolve()
      } else {
        requestAnimationFrame(update)
      }
      const level = calculateLevel(base + delta)
      const length = getRequired(level) - getRequired(level - 1)
      const value = base + delta - getRequired(level - 1)
      progress.value = value
      progress.max = length
    }
    requestAnimationFrame(update)
    save()
  })
}
