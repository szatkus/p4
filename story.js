import { fight, RESULT_QUIT, RESULT_LOSE, RESULT_WIN } from './battle.js'
import { clear, showList, whiteOut, makeDisappear, showBigText, refreshHealth, setTitle, showDescription, mergeElements, tell, showNo, showYes, showButtons } from './ui.js'
import { get, set } from './variables.js'
import { addExperience, recalculate } from './levels.js'
import { registerPlace, goto } from './places.js'
import { recover, save } from './character.js'

const digits = ['zero', 'one', 'two', 'three', 'four', 'five', 'six']

function catchTitle(handler) {
  return async function ({ selected }) {
    setTitle(selected.textContent)
    await mergeElements(selected, document.getElementById('title'), 500)
    handler()
  }
}

function eat(handler) {
  return async function ({ selected }) {
    await makeDisappear(selected, 200)
    handler()
  }
}

registerPlace('beginning', async () => {
  setTitle('Bed Chamber')
  await showList([{
    label: 'Body',
    action: catchTitle(async () => {
      await showDescription("It lacks a soul, but you can feel a sliver of life. It was devoided very recently.")
      goto()
    })
  }, {
    label: 'Bed',
    action: catchTitle(async () => {
      await showDescription([
        "It doesn't quite resemble a  bed. More like one of these sleep pods that are used in movies during long space voyages.",
        "Maybe you're on another planet?",
        "Other than what planet?"
      ])
      goto()
    })
  }, generateSingleUseObject('Metal Pipe', 'metalPipe', () => {}), {
    label: 'Corridor',
    action: catchTitle(async () => goto('structureCorridor'))
  }])
})
registerPlace('structureCorridor', async () => {
  setTitle('Corridor')
  await showList([{
    label: 'Bed Chamber',
    action: catchTitle(async () => goto('beginning'))
  }, {
    label: 'Door',
    isVisible: () => !get('structureHall'),
    lights: ['button1', 'button2', '!button3'],
    action: catchTitle(async () => {
      let counter = 0
      if (get('button1')) {
        counter++
      }
      if (get('button2')) {
        counter++
      }
      if (!get('button3')) {
        counter++
      }
      if (counter === 3) {
        await showDescription("It's open!")
        addExperience(1)
        set('structureHall')
      } else {
        await showDescription(["It's closed.", `There are ${digits[counter]} lights on it.`])
      }
      goto()
    })
  }, {
    label: 'Hall',
    action: catchTitle(() => goto('structureHall')),
    isVisible: () => get('structureHall')
  }, {
    label: 'Hole',
    isVisible: () => !get('controlRoom'),
    action: catchTitle(async () => {
      if (get('metalPipe')) {
        await showDescription("You put the pipe into it.")
        setTitle('')
        await showList([
          {
            label: 'Turn',
            action: async ({ selected }) => {
              await makeDisappear(selected)
              await showDescription("You've opened a passage to another room.")
              addExperience(1)
              set('controlRoom')
            }
          },
          {
            label: 'Put out',
            action: makeDisappear
          }
        ])
      } else {
        await showDescription([
          "There's a hole in the wall.",
          "Looks like some kind of mechanism.",
          "If you only had a thin, cylindrical object that could be in another location..."
        ])
      }
      goto()
    })
  }, {
    label: 'Control Room',
    isVisible: () => get('controlRoom'),
    action: catchTitle(async () => goto('controlRoom'))
  }])
})
registerPlace('controlRoom', async () => {
  setTitle('Control Room')
  await showList([
    {
      label: 'Button',
      toggle: 'button1'
    }, {
      label: 'Button',
      toggle: 'button2'
    }, {
      label: 'Button',
      toggle: 'button3'
    }, {
      label: 'Sign',
      action: catchTitle(async () => {
        await showDescription('SHŌ')
        goto()
      })
    }, {
      label: 'Corridor',
      action: catchTitle(async () => goto('structureCorridor'))
    }
  ])
})
registerPlace('structureHall', async () => {
  setTitle('Hall')
  await showList([
    {
      label: 'Exit',
      lights: ['crystal1', 'crystal2', 'crystal3'],
      action: eat(async () => {
        if (get('chapter1')) {
          goto('summit')
          return
        }
        if (get('crystal1') && get('crystal2') && get('crystal3')) {
          addExperience(1)
          goto('prolog')
        } else {
          await showDescription("It's closed")
          goto()
        }
      })
    }, {
      label: 'Crystal',
      action: catchTitle(async () => {
        if (!get('crystal1')) {
          addExperience(1)
        }
        set('crystal1')
        await showDescription("You ignited the crystal.")
        goto()
      })
    }, {
      label: 'Sign',
      action: catchTitle(async () => {
        await showDescription('MIMI')
        goto()
      })
    }, {
      label: 'Left Chamber',
      action: catchTitle(async () => {
        if (!get('firstAnomaly')) {
          await showDescription([
            "In the next room there is an anomaly running around.",
            "You can't percieve its appearance as it is a creature living in a gap between realms, but you noticed that fortunately it's rather nimble.",
            "It's running in your direction."
          ])
          set('firstBattle')
          const result = await fight([
            {
              name: 'Small Anomaly',
              health: 5,
              attack: 5,
              defense: 5,
              speed: 5
            }
          ], 3)
          if (result === RESULT_QUIT || result === RESULT_LOSE) {
            save()
            goto('beginning')
          } else {
            set('firstAnomaly')
            goto('leftChamber')
          }
        } else {
          goto('leftChamber')
        }
      })
    }, {
      label: 'Right Chamber',
      action: catchTitle(() => goto('rightChamber'))
    }, {
      label: 'Corridor',
      action: catchTitle(async () => goto('structureCorridor'))
    }
  ])
})
registerPlace('leftChamber', async () => {
  setTitle('Left Chamber')
  await showList([
    {
      label: 'Crystal',
      action: catchTitle(async () => {
        if (!get('crystal2')) {
          addExperience(1)
        }
        set('crystal2')
        await showDescription('You ignited the crystal.')
        goto()
      })
    },
    {
      label: 'Sign',
      action: catchTitle(async () => {
        await showDescription('GATA')
        goto()
      })
    }, {
      label: 'Hall',
      action: catchTitle(async () => goto('structureHall'))
    }
  ])
})
registerPlace('rightChamber', async () => {
  setTitle('Right Chamber')
  await showList([
    {
      label: 'Lever I',
      action: catchTitle(async () => {
        set('option1', false)
        await showList([
          {
            label: 'KATA'
          },
          {
            label: 'GATA',
            action: () => set('option1', true)
          },
          {
            label: 'BATA'
          },
          {
            label: 'HATA'
          }
        ], { hideSelection: true })
        goto()
      })
    }, {
      label: 'Lever II',
      action: catchTitle(async () => {
        set('option2', false)
        await showList([
          {
            label: 'CHŌ'
          },
          {
            label: 'RYŌ'
          },
          {
            label: 'HŌ'
          },
          {
            label: 'SHŌ',
            action: () => set('option2', true)
          }
        ], { hideSelection: true })
        goto()
      })
    }, {
      label: 'Lever III',
      action: catchTitle(async () => {
        set('option3', false)
        await showList([
          {
            label: 'MIMI',
            action: () => set('option3', true)
          },
          {
            label: 'BIBI'
          },
          {
            label: 'HIHI'
          },
          {
            label: 'DIDI'
          }
        ], { hideSelection: true })
        goto()
      })
    }, {
      label: 'Crystal',
      action: catchTitle(async () => {
        if (get('option1') && get('option2') && get('option3')) {
          if (!get('crystal3')) {
            addExperience(1)
          }
          set('crystal3')
          await showDescription('You ignited the crystal.')
        } else {
          await showDescription('The crystal is enclosed in a cage.')
        }
        goto()
      })
    }, {
      label: 'Hall',
      action: catchTitle(async () => goto('structureHall'))
    }
  ])
})

registerPlace('prolog', async () => {
  await showNo()
  await showDescription([
    "You're outside.",
    "Everything out there is black and white.",
    "Isn't it normal?",
    "Was there ever any other color?",
    "..."
  ])
  await showDescription([
    "There's a person nearby.",
    "They contain an old soul. You can sense it. Probably they live here since the beginning of the plane.",
  ])
  await tell("Elder", "Did you just come out of the Structure?")
  await tell("", "...")
  await tell("Elder", "You can't talk?")
  setTitle('')
  await showDescription("You shaked your head.")
  await showNo()
  await tell("Elder", [
    "I see.",
    "But that's a avery important question. Did you come out of the Structure or not? I really need to know that."
  ])
  await showYes()
  await tell("Elder", [
    "That's very concerning.",
    "It was said that once a man come out of the Structure, the entire plane will face annihilation.",
    "And this is you. You came from the Structure. Why would you do that?",
    "Why do you want to destroy our world!?"
  ])
  const canvas = await whiteOut()
  await showBigText("Chapter I")
  clear()
  await makeDisappear(canvas)
  set('chapter1')
  goto('summit')
})

const smallAnomaly = {
  name: 'Small Anomaly',
  health: 5,
  attack: 5,
  defense: 5,
  speed: 5,
  accuracy: 15,
  evasion: 1
}

const mediumAnomaly = {
  name: 'Medium Anomaly',
  health: 18,
  attack: 13,
  defense: 5,
  speed: 8,
  accuracy: 20,
  evasion: 2
}

const swiftAnomaly = {
  name: 'Swift Anomaly',
  health: 10,
  attack: 5,
  defense: 1,
  speed: 10,
  accuracy: 100,
  evasion: 100
}

registerPlace('summit', async () => {
  setTitle('Summit')
  await showList([
    {
      label: 'The Structure',
      action: eat(() => goto('structureHall'))
    }, {
      label: 'Block A',
      action: catchTitle(async () => {
        const result = await fight([mediumAnomaly], 6)
        if (result === RESULT_WIN) {
          goto('blockA')
        } else {
          goto('summit')
        }
        save()
      })
    }, {
      label: 'Block B',
      action: catchTitle(() => goto('blockB'))
    }, {
      label: 'Block C'
    }, {
      label: 'Block D'
    }, {
      label: 'Orb',
      action: catchTitle(async () => {
        recover()
        refreshHealth()
        save()
        await showDescription('Recovery...')
        goto()
      })
    },
    generateSingleUseObject('B-Stone', 'bstone'),
    {
      label: 'Down',
      action: eat(() => showList([
        {
          label: 'Cancel',
          action: () => goto('summit')
        },
        {
          label: 'IIIIIIIIIIIIIII',
          action: eat(async () => {
            const result = await fight([smallAnomaly], 3)
            if (result === RESULT_WIN) {
              set('summit15')
            }
            save()
            goto('summit')
          })
        },
        {
          label: 'IIIIIIIIIIIIII',
          isVisible: () => get('summit15'),
          action: eat(async () => {
            const result = await fight([smallAnomaly, smallAnomaly], 7)
            if (result === RESULT_WIN) {
              set('summit14')
            }
            save()
            goto('summit')
          })
        },
        {
          label: 'IIIIIIIIIIIII',
          isVisible: () => get('summit14'),
          action: eat(async () => {
            const result = await fight([smallAnomaly, smallAnomaly, smallAnomaly], 11)
            if (result === RESULT_WIN) {
              set('summit13')
              await showDescription("You found an A-Stone.")
              set(get('astone') + 1)
            }
            save()
            goto('summit')
          })
        },
        {
          label: 'IIIIIIIIIIII',
          isVisible: () => get('summit13') && !get('summit12'),
          action: eat(async () => {
            await tell('Elder', [
              'The reality is breaking apart.',
              'When I came here this trail had just a few levels, now there are like 15!',
              'Why are you doing this to us?'
            ])
            set('summit12')
            save()
            goto('summit')
          })
        },
        {
          label: 'IIIIIIIIIII',
          isVisible: () => get('summit12'),
          action: eat(async () => {
            await showDescription("There's something really fast...")
            const result = await fight([swiftAnomaly], 6)
            if (result === RESULT_WIN) {
              set('summit11')
            } else {
              await showDescription("There must be a way to defeat that thing...")
            }
            save()
            goto('summit')
          })
        }
      ]))
      }
    ])
})


registerPlace('blockA', async () => {
  setTitle('Block A')
  await showList([
    generateFight('Storage', [mediumAnomaly], 6, 'storage', 'summit', 'blockA'),
    generateFight('Corridor', [mediumAnomaly, mediumAnomaly], 15, 'corridor', 'summit', 'blockA'),
    generateSingleUseObject('Chain', 'chain', () => {}), {
      label: 'Exit',
      action: eat(() => goto('summit'))
    }
  ])
})

registerPlace('blockB', async () => {
  setTitle('Block B')
  await showList([
    generateBuyable('+10 Health', 'health10', 'Adds 10 maximum health points.', {bstone: 2}), {
      label: 'Exit',
      action: eat(() => goto('summit'))
    }
  ])
})

registerPlace('storage', async () => {
  await showList([
    {
      label: 'Note',
      action: catchTitle(async () => {
        await showDescription([
          "It's a note.",
          "Unfortunately you don't know how to read.",
          "You probably never have.",
          "Actually you don't even know how a writing looks like, so it may not be a note but a drawing."
        ])
        goto()
      })
    },
    generateSingleUseObject('A-Stone', 'astone'),
    {
      label: 'Back',
      action: eat(() => goto('blockA'))
    }
  ])
})

registerPlace('corridor2', async () => {
  await showList([
    generateSingleUseObject('B-Stone', 'bstone'),
    {
      label: 'Emergency Exit',
      action: eat(() => goto('summit'))
    },
    generateFight('Back', [mediumAnomaly, mediumAnomaly], 6, 'corridor', 'summit', 'corridor2'),
  ])
})

registerPlace('corridor', async () => {
  await showList([
    generateFight('Forward', [mediumAnomaly, mediumAnomaly], 15, 'corridor2', 'summit', 'corridor'),
    {
      label: 'Emergency Exit',
      action: eat(() => goto('summit'))
    },
    generateFight('Back', [mediumAnomaly], 6, 'blockA', 'summit', 'corridor'),
  ])
})


function generateSingleUseObject(label, name) {
  const id = `${name}_${get('place')}`
  return {
    label: `[${label}]`,
    isVisible: () => !get(id),
    justDisappear: true,
    action: async () => {
      const value = +get(name)
      if (!value) {
        set(name, 1)
      } else {
        set(name, value + 1)
      }
      set(id)
    }
  }
}

function generateFight(label, enemies, xp, win, lose, quit) {
  return {
    label,
    action: catchTitle(async () => {
      const result = await fight(enemies, xp)
      if (result === RESULT_WIN) {
        goto(win)
      }
      if (result === RESULT_LOSE) {
        goto(lose)
      }
      if (result === RESULT_QUIT) {
        goto(quit)
      }
      save()
    })
  }
}

function generateBuyable(label, name, description, requirements) {
  return {
    label,
    isVisible: () => !get(name),
    action: catchTitle(async () => {
      const text = [description, '<b>Requirements:</b>']
      let isEnough = true
      for (const name in requirements) {
        if (name.indexOf('stone') === 1) {
          const symbol = name[0].toUpperCase()
          const amount = parseInt(get(name))
          text.push(`${symbol} stones: ${amount}/${requirements[name]}`)
          if (amount < requirements[name]) {
            isEnough = false
          }
        }
      }
      showButtons(() => {
        if (isEnough) {
          for (const name in requirements) {
            const amount = parseInt(get(name))
            set(name, amount - requirements[name])
          }
          set(name, true)
          recalculate()
          save()
          refreshHealth()
          clear()
          goto()
          return true
        }
      }, () => {
        clear()
        goto()
        return true
      })
      await showDescription(text, 'print')
      
    })
  }
}
