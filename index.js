import { refreshExperience, refreshHealth, wait, showDescription } from './ui.js'
import { registerPlace, goto } from './places.js'

import {} from './story.js'

refreshExperience()
refreshHealth()
if ('place' in localStorage) {
  goto(localStorage.getItem('place'))
} else {
  await wait(1000)
  await showDescription([`You woke up.`, `You find yourself in a dark empty room. You can't remember anything. There is a body on the floor.`, `What happened?`], 'fade')
  goto('beginning')
}

function addHealth (increase) {
  const velocity = Math.max(increase / ANIMATION_DURATION, 0.01)
  const element = document.getElementById('maxHealth')
  const base = maxHealth
  maxHealth += increase

  const start = +new Date()
  function update () {
    let delta = (new Date() - start) * velocity
    if (delta > increase) {
      delta = increase
    } else {
      requestAnimationFrame(update)
    }
    const current = base + Math.round(delta)
    element.innerText = current
  }
  requestAnimationFrame(update)
}

