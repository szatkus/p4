import { player, save, recover } from './character.js'
import { addExperience } from './levels.js'
import { refreshHealth, showList, mergeElements } from './ui.js'
import { get } from './variables.js'

const INITIATIVE_THRESHOLD = 1000
const VISIBLE_MOVES = 10

export const RESULT_QUIT = Symbol('quit')
export const RESULT_LOSE = Symbol('lose')
export const RESULT_WIN = Symbol('win')
export const MISS = Symbol('miss')

const romanNumbers = ['I', 'II', 'III', 'IV', 'V']

export async function fight (enemies, experience) {
  const tempo = 500
  enemies = enemies.map(e => Object.create(e))
  const counts = new Map()
  for (const enemy of enemies) {
    counts.set(enemy.name, (counts.get(enemy.name) ?? 0) + 1)
    enemy.maxHealth = enemy.health
  }
  const counts2 = new Map()
  for (const enemy of enemies) {
    if (counts.get(enemy.name) > 1) {
      const currentValue = counts2.get(enemy.name) ?? 0
      counts2.set(enemy.name, currentValue + 1)
      enemy.name += ' ' + romanNumbers[currentValue]
    }
  }
  const participants = [player].concat(enemies)
  for (const p of participants) {
    p.initiative = Math.round(Math.random() * INITIATIVE_THRESHOLD / 2)
  }

  let nextMoves = []
  const output = document.getElementById('output')
  const container = document.createElement('div')
  Object.assign(container.style, {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'column',
    transition: 'opacity 200ms'
  })
  output.appendChild(container)

  await wait(tempo)

  await refillMoves()

  const dead = []
  while (player.health > 0) {
    while (participants.filter(p => p !== player && p.health > 0).length > 0) {
      const current = nextMoves.shift()
      if (current.participant.restrained) {
        current.participant.restrained--
      }
      if (current.participant === player) {
        const listContainer = document.createElement('div')
        Object.assign(listContainer.style, {
          height: '0',
          width: 'calc(100% - 10px)',
          padding: '11px 6px',
          transition: 'height 300ms, padding 300ms, opacity 300ms',
          overflow: 'hidden'
        })
        const you = current.label
        container.insertBefore(listContainer, container.children[1])

        const result = await showList([{
          label: 'Attack',
          sublist: listEnemies(executeAction((player, enemy) => {
            const damage = attack(player, enemy)
            return damage === MISS ? ' evaded the attack' : ` lost ${damage} ${pointsForm(damage)}`
          }))
        }, {
          label: 'Chain',
          isVisible: () => get('chain'),
          sublist: listEnemies(async (enemy, actionItem, targetItem) => {
            you.innerHTML = 'You use '
            return executeAction((player, enemy) => {
              enemy.restrained = 2
              return " is restrained"
            })(enemy, actionItem, targetItem)
          })
        }, {
          label: 'Quit',
          action: ({ selected }) => {
            selected.remove()
            return RESULT_QUIT
          }
        }], {
          parent: listContainer,
          slide: false,
          target: listContainer,
          onInit: (element) => {
            const height = element.getBoundingClientRect().height
            listContainer.style.height = height + 'px'
            setTimeout(() => listContainer.style.height = '100%', tempo)
          }
        })
        if (result === RESULT_QUIT) {
          container.style.opacity = 0
          await wait(tempo)
          output.innerHTML = ''
          return RESULT_QUIT
        }

        function executeAction (execute) {
          return async (enemy, actionItem, targetItem) => {
            const size = listContainer.getBoundingClientRect().height - 22
            listContainer.style.height = `${size}px`
            listContainer.style.transition = '' // it flickers without it
            await wait(10)
            const actionContainer = document.createElement('span')
            you.appendChild(actionContainer)
            mergeElements(actionItem, actionContainer, tempo)
            current.element.style.borderColor = 'rgba(0, 0, 0, 0)'
            current.bar.style.opacity = 0
            
            listContainer.style.textAlign = 'left'
            listContainer.innerHTML = ''
            const actionResult = document.createElement('span')
            listContainer.appendChild(actionResult)
            mergeElements(targetItem, actionResult, tempo)
            listContainer.style.transition = `height ${tempo}ms, padding ${tempo}ms, opacity ${tempo}ms`
            listContainer.style.height = `${actionResult.getBoundingClientRect().height}px`
      
            await wait(tempo)
            const message = execute(player, enemy)
            const additionalText = document.createElement('span')
            additionalText.style.fontWeight = 'normal'
            actionResult.appendChild(additionalText)
            updateHealth()
            typeMessage(additionalText, message)
            await wait(1000)
            listContainer.style.height = 0
            listContainer.style.padding = 0
            listContainer.style.height = `${listContainer.getBoundingClientRect().height}px`
            setTimeout(() => listContainer.style.height = 0, 1)
            current.element.style.overflow = 'hidden'
            current.element.style.height = `${current.element.getBoundingClientRect().height}px`
            current.element.style.margin = 0
            setTimeout(() => current.element.style.height = 0, 1)
            await wait(tempo)
            container.removeChild(listContainer)
            container.removeChild(current.element)
          }
        }
      } else {
        current.element.style.borderColor = 'rgba(0, 0, 0, 0)'
        current.bar.style.opacity = 0
        if (current.participant.restrained) {
          await typeMessage(current.label, " can't move")
          await wait(tempo)
        } else {
          typeMessage(current.label, ' attacks')
          current.element.style.height = `${current.element.getBoundingClientRect().height}px`
          await wait(1)
          current.element.style.height = `${current.element.getBoundingClientRect().height * 2}px`
          await wait(tempo)
          const actionResult = document.createElement('div')
          Object.assign(actionResult.style, {
            width: `calc(100% - 10px)`,
            padding: '10px 5px'
          })
          current.element.appendChild(actionResult)
          const damage = attack(current.participant, player)
          updateHealth()
          if (damage === MISS) {
            await typeMessage(actionResult, `${current.participant.name} missed`, { effect: 'slide', timeout: 400 })
          } else {
            await typeMessage(actionResult, `You lost ${damage} ${pointsForm(damage)}`, { effect: 'slide', timeout: 600 })
          }          
        }
        await wait(tempo)
        current.element.style.overflow = 'hidden'
        current.element.style.margin = 0
        current.element.style.height = `${current.element.getBoundingClientRect().height}px`
        await wait(1)
        current.element.style.height = 0
        await wait(tempo)
        container.removeChild(current.element)
      }

      for (const p of participants) {
        if (p.health <= 0 && !dead.includes(p) && p.name) {
          const toBeRemoved = nextMoves.filter(m => m.participant === p)
          for (const move of toBeRemoved) {
            move.element.style.opacity = 0
          }
          await wait(tempo)
          for (const move of toBeRemoved) {
            move.element.style.height = `${move.element.getBoundingClientRect().height}px`
          }
          await wait(1)
          for (const move of toBeRemoved) {
            move.element.style.height = 0
            move.element.style.margin = 0
          }
          dead.push(p)
          await wait(tempo)
          for (const move of toBeRemoved) {
            container.removeChild(move.element)
          }
          nextMoves = nextMoves.filter(m => m.participant !== p)
        }
      }

      

      const alive = participants.filter(p => p.health > 0)
      if (alive.length === 1 && alive[0] === player) {
        container.style.opacity = 0
        await wait(200)
        output.innerHTML = ''
        await addExperience(experience)
        save()
        return RESULT_WIN
      } else {
        await refillMoves()
      }
    }
  }

  recover()
  refreshHealth()
  return RESULT_LOSE

  async function typeMessage(element, message) {
    for (const c of message) {
      element.innerHTML += c
      await wait(40)
    }
  }

  function attack (offender, target) {
    const off = Math.random() * offender.accuracy
    const def = target.restrained ? 0 : target.evasion
    if (off < def) {
      return MISS
    }
    const damage = Math.max(Math.round((0.95 + Math.random() * 0.1) * (offender.attack - (target.defense || 0))), 1)
    target.health -= damage
    refreshHealth()
    return damage
  }

  async function refillMoves () {
    return new Promise((resolve) => {
      const newMoves = []
      while (nextMoves.length < VISIBLE_MOVES) {
        for (const p of participants) {
          if (p.health > 0) {
            p.initiative += p.speed
            if (p.initiative > INITIATIVE_THRESHOLD) {
              p.initiative -= INITIATIVE_THRESHOLD
              const move = {
                participant: p
              }
              nextMoves.push(move)
              newMoves.push(move)
            }
          }
        }
      }

      let delay = 0

      for (const move of newMoves) {
        const element = document.createElement('div')
        const participant = move.participant
        Object.assign(element.style, {
          width: 'calc(100%)',
          position: 'relative',
          border: `1px solid black`,
          margin: '5px 0',
          textAlign: 'left',
          transition: 'borderColor 500ms, height 300ms, margin 300ms, opacity 300ms'
        })
    
        const bar = document.createElement('div')
        Object.assign(bar.style, {
          width: `calc(${100 * participant.health / participant.maxHealth}% - 10px)`,
          backgroundColor: '#c4c4c4',
          transition: 'width 200ms, opacity 200ms',
          padding: '10px 5px',
          whiteSpace: 'nowrap'
        })
        bar.textContent = participant.name ? participant.name : 'You '
        element.appendChild(bar)
        
        const label = document.createElement('div')
        Object.assign(label.style, {
          position: 'absolute',
          top: '10px',
          left: '5px',
        })
        label.textContent = bar.textContent
        element.appendChild(label)
    
        move.element = element
        move.label = label
        move.bar = bar
    
        container.appendChild(element)
        element.style.opacity = 0
        setTimeout(() => element.style.opacity = 1, delay)
        delay += 50
      }
      setTimeout(resolve, delay)
    })
  }

  function listEnemies (action) {
    return enemies.filter(e => e.health > 0).map(e => ({
      label: e.name,
      action: ({ parentItem, selected }) => action(e, parentItem, selected)
    }))
  }

  function updateHealth () {
    for (const move of nextMoves) {
      if (move.participant.health > 0) {
        move.bar.style.width = `calc(${100 * move.participant.health / move.participant.maxHealth}% - 10px)`
      } else {
        move.bar.style.width = 0
        move.bar.style.padding = '10px 0px'
        move.bar.style.color = 'rgba(0, 0, 0, 0)'
      }
    }
  }
}

function pointsForm(points) {
  if (points === 1) {
    return 'point'
  }
  return 'points'
}

function wait (time) {
  return new Promise((resolve) => {
    setTimeout(() => resolve(), time)
  })
}
