const places = {
}

export function goto (name) {
  if (name) {
    localStorage.setItem('place', name)
  } else {
    name = localStorage.getItem('place')
  }
  places[name]()
}

export function registerPlace (name, definition) {
  if (places.hasOwnProperty(name)) {
    throw new Error('This place already exists.')
  }
  places[name] = definition
}
